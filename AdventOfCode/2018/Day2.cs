﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day2
    {
        public static void Run()
        {
            var input = File.ReadAllLines(@"2018/Input/Day2.txt");

            var twice = 0;
            var three = 0;

            foreach(var id in input)
            {
                var a = id.ToCharArray().ToList();

                var grouped = a.GroupBy(x => x);

                twice += grouped.Any(x => x.Count() == 2) ? 1 : 0;
                three += grouped.Any(x => x.Count() == 3) ? 1 : 0;

            }

            Console.WriteLine($"Answer 1: {twice * three}");
            
            foreach(var id in input)
            {
                foreach(var id2 in input.Where(x => x != id))
                {
                    var differences = 0;
                    var firstDiff = "";
                    if (id.Length != id2.Length)
                        continue;

                    for (var i = 0; i < id.Length;i++)
                    {
                        if(id[i] != id2[i])
                        {
                            differences++;
                            if (differences == 1)
                            {
                                firstDiff = id[i].ToString();
                            }
                        }

                    }

                    if(differences == 1)
                    {
                        Console.WriteLine($"Answer 2: {id.Replace(firstDiff,"")}");
                        return;
                    }
                }
            }
        }
    }
}
