﻿using System;

namespace AdventOfCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Day13.Run();
            
            Console.ReadLine();
        }
    }
}
