﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day3
    {
        public static void Run()
        {
            var input = File.ReadAllLines(@"../../../../Input/Day3.txt");

            var requests = input.Select(x => new Request(x)).ToList();
            var overlap = new HashSet<(int,int)>();

            requests.ForEach(x => CreateGrid(x));

            var grids = requests.SelectMany(x => x.Grid).ToList();
            var grouped = grids.GroupBy(x => x);

            var a = grids.GroupBy(c => c)
                            .Where(grp => grp.Count() > 1)
                            .Select(grp => grp.Key).ToList();

            Console.WriteLine($"Answer 1: {a.Count()}");

            requests.ForEach(x =>
            {
                if (!x.Grid.Any(g => a.Contains(g)))
                {
                    Console.WriteLine($"Answer 2: {x.Number}");
                }
            });
        }

        private static void CreateGrid(Request request)
        {
            for (var w = 0; w < request.Width; w++)
            {
                for (var h = 0; h < request.Height; h++)
                {
                    request.Grid.Add((request.Left + w, request.Top + h));                    
                }                
            }            
        }
    }

    public class Request
    {
        public int Number { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public HashSet<(int, int)> Grid { get; set; } = new HashSet<(int, int)>();

        public Request(string values)
        {
            Number = int.Parse(values.Substring(1, values.IndexOf(" ") - 1));
            Left = int.Parse(values.Substring(values.IndexOf("@") + 2, values.IndexOf(",") - values.IndexOf("@") - 2));
            Top = int.Parse(values.Substring(values.IndexOf(",") + 1, values.IndexOf(":") - values.IndexOf(",") - 1));
            Width = int.Parse(values.Substring(values.IndexOf(":") + 2, values.IndexOf("x") - values.IndexOf(":") - 2));
            Height = int.Parse(values.Substring(values.IndexOf("x") + 1, values.Length - values.IndexOf("x") - 1));
        }
    }
}
