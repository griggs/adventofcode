﻿using System;
namespace AdventOfCode
{
    public static class Day11
    {
       
        public static void Run()
        {
            var input = 9005;
            //CalculateCell((3,5));

            var grid = new Cell[300, 300];
            var largest = 0;
            var largestGridSize = 0;
            (int x, int y) Answer1 = (0,0);


            for (var x = 0; x < 300; x++)
            {
                for (var y = 0; y < 300; y++)
                {
                    var i = new Cell();
                    i.Coords = (x+1,y+1);
                    i.Power = CalculateCell(i);
                    grid[x, y] = i;


                }
            }

            for (var gridSize = 1; gridSize <= 300; gridSize++)
            {
                if (gridSize % 10 == 0)
                {
                    Console.WriteLine($"gridSize: {gridSize}");
                    Console.WriteLine($"Answer 1: {(Answer1.x).ToString()},{(Answer1.y).ToString()},{largestGridSize}");
                }

                for (var x = 0; x< 300; x++)
                {
                    for (var y = 0; y< 300; y++)
                    {
                        if(!(x + gridSize > 300) && !(y + gridSize > 300))
                        {
                            var value = Calculate(grid[x, y], gridSize);
                            if (value > largest)
                            {
                                largest = value;
                                largestGridSize = gridSize;
                                Answer1 = (grid[x, y].Coords.x, grid[x, y].Coords.y);
                            }
                        }
                    }
                }
            }

            Console.WriteLine($"TotalPower: {largest}");
            Console.WriteLine($"Answer 1: {(Answer1.x + 1).ToString()},{(Answer1.y + 1).ToString()},{largestGridSize}");


            int Calculate(Cell cell, int gridSize)
            {
                var total = 0;

                for (var x = 0; x < gridSize; x++)
                {
                    for (var y = 0; y < gridSize; y++)
                    {
                        total += grid[cell.Coords.x - 1 + x, cell.Coords.y -1 + y].Power;
                    }
                }

                return total;
            }

            int CalculateCell(Cell cell)
            {
                //Find the fuel cell's rack ID, which is its X coordinate plus 10.
                var rackID = cell.Coords.x + 10;
                //Begin with a power level of the rack ID times the Y coordinate.
                var power = rackID * cell.Coords.y;
                //Increase the power level by the value of the grid serial number(your puzzle input).
                power += input;
                //Set the power level to itself multiplied by the rack ID.
                power = power * rackID;
                //Keep only the hundreds digit of the power level(so 12345 becomes 3; numbers with no hundreds digit become 0).
                power = power < 100 ? 0 : (int)Math.Abs(power / 100 % 10);
                //Subtract 5 from the power level.
                power -= 5;

                return power;
            }

        }
    }

    class Cell
    {
        public (int x,int y) Coords { get; set; }
        public int Power { get; set; }
    }
}


//for (var x = 0; x< 300; x++)
            //{
            //    for (var y = 0; y< 300; y++)
            //    {

            //        var value = Calculate((x, y), gridSize);
            //        if (value > largest)
            //        {
            //            largest = value;
            //            largestGridSize = gridSize;
            //            Answer1 = (x, y);
            //        }
                    
            //    }
            //}