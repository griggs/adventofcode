﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day9
    {
        public static void Run()
        {
            var input = File.ReadAllLines(@"../../../../Input/Day9.txt").First();
            //var input = File.ReadAllLines(@"../Input/Day9.txt").First();

            var playerCount = int.Parse(input.Substring(0, input.IndexOf(' ')));
            var maxMarble = int.Parse(input.Substring(input.IndexOf("worth") + 6, input.IndexOf(" points") - (input.IndexOf("worth") + 6)));
            //part 2
            maxMarble = maxMarble * 100;
            var currentMarbleIndex = 0;
            var currentPlayer = 0;
            var marbles = new List<int>();
            var players = new List<Player>();

            for(var i = 0;i<playerCount;i++)
            {
                players.Add(new Player() { Number = i + 1 });
            }

            marbles.Insert(0, 0);
            currentMarbleIndex = 0;

            for (var marble = 1; marble <= maxMarble; marble++)
            {
                currentPlayer++;
                if(currentPlayer > playerCount)
                {
                    currentPlayer = 1;
                }

                if(marble % 23 == 0)
                {
                    var marbleToRemove = currentMarbleIndex - 7;
                    if(marbleToRemove < 0)
                    {
                        marbleToRemove = marbleToRemove + marbles.Count;
                    }
                    var p = players.Single(x => x.Number == currentPlayer);
                    p.Marbles.Add(marble);
                    p.Marbles.Add(marbles[marbleToRemove]);
                    marbles.RemoveAt(marbleToRemove);
                    currentMarbleIndex = marbleToRemove;

                    continue;
                }

                var position = currentMarbleIndex + 2;
                if(position > marbles.Count)
                {
                    position -= marbles.Count;
                }

                marbles.Insert(position, marble);
                currentMarbleIndex = position;
            }

            Console.WriteLine($"Answer 2: {players.Max(x => x.Score())}");
        }
    }

    class Player
    {
        public int Number { get; set; }
        public List<Int64> Marbles { get; set; } = new List<Int64>();
        public Int64 Score() => Marbles.Sum();
    }

}
