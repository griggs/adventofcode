﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day13
    {
        public static void Run()
        {
            var input = File.ReadAllLines(@"../../../../Input/Day12.txt");
            //var input = File.ReadAllLines(@"../Input/Day12Test.txt");
            var lineCount = input.Count();
            var columnCount = input.Max(x => x.Length);
            var track = new string[lineCount,columnCount];
            
            
            

            //Console.WriteLine($"Answer 1: {pots.Where(x => x.Value == "#").Sum(x => x.Index)}");

            //Debugger.Break();

        }
    }
    enum Direction
    {
        Left,
        Straight,
        Right
    }

    class Minecart
    {
        public (int x, int y) Position { get; set; }
        public string CurrentDirection { get; set; }
        public Direction NextIntersection { get; set; }
    }

    class Rule
    {
        public string Output { get; set; }
        public string Current { get; set; }
        public string One { get; set; }
        public string Two { get; set; }
        public string Four { get; set; }
        public string Five { get; set; }
        public List<(int postion, string value)> Ruleset { get; set; } = new List<(int postion, string value)>();

        public Rule(string line)
        {
            Output = line.Substring(line.IndexOf("=> ") + 3).Trim();
            Current = line.Substring(2, 1);
            One = line.Substring(0, 1);
            Two = line.Substring(1, 1);
            Four = line.Substring(3, 1);
            Five = line.Substring(4, 1);

            Ruleset.Add((-2, line.Substring(0,1)));
            Ruleset.Add((-1, line.Substring(1, 1)));
            Ruleset.Add((1, line.Substring(3, 1)));
            Ruleset.Add((2, line.Substring(4, 1)));
        }
    }
}
