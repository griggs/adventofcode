﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day10
    {
        public static void Run()
        {
            //var input = File.ReadAllLines(@"../../../../Input/Day10Test.txt");
            var input = File.ReadAllLines(@"../Input/Day10.txt");

            var points = input.ToList().Select(x => new Coord(x)).ToList();

            var lastSmallest = int.MaxValue;
            var moves = 0;
            while(true)
            {
                points.ForEach(x => x.Move());
                moves++;
                var area = CurrentArea();
                if(area <= lastSmallest)
                {
                    lastSmallest = area;
                }
                else
                {
                    points.ForEach(x => x.MoveBack());
                    break;
                }
            }

            var maxX = points.Max(x => x.Position.x);
            var maxY = points.Max(x => x.Position.y);
            var minX = points.Min(x => x.Position.x);
            var minY = points.Min(x => x.Position.y);

            for (var y = minY; y <= maxY; y++)
            {
                for (var x = minX; x <= maxX; x++)
                {
                    if(points.Any(p => p.Position.x == x && p.Position.y == y))
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine($"Answer 2: {moves -1}");

            int CurrentArea()
            {
                var a = points.Max(x => x.Position.x);
                var b = points.Max(x => x.Position.y);
                var c = points.Min(x => x.Position.x);
                var d = points.Min(x => x.Position.y);

                return (Math.Abs(a + c) * Math.Abs(b + d));
            }

        }
    }

    class Coord
    {
        public (int x, int y) Position { get; set; }
        public (int x, int y) Velocity { get; set; }

        public void Move()
        {
            Position = (Position.x + Velocity.x, Position.y + Velocity.y);           
        }

        public void MoveBack()
        {
            Position = (Position.x - Velocity.x, Position.y - Velocity.y);
        }


        public Coord(string line)
        {
            var x = int.Parse(line.Substring(line.IndexOf("position=<") + 10, line.IndexOf(",") - (line.IndexOf("position=<") + 10)));
            var y = int.Parse(line.Substring(line.IndexOf(",") + 1, line.IndexOf(">") - (line.IndexOf(",") + 1)));

            Position = (x, y);

            var xv = int.Parse(line.Substring(line.IndexOf("velocity=<") + 10, line.LastIndexOf(",") - (line.IndexOf("velocity=<") + 10)));
            var yv = int.Parse(line.Substring(line.LastIndexOf(",") + 1, line.LastIndexOf(">") - (line.LastIndexOf(",") + 1)));
            Velocity = (xv,yv);
           
        }
    }

}
