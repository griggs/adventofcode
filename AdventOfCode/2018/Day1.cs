﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace AdventOfCode
{
    public static class Day1
    {
        public static void Run()
        {
            var values = File.ReadAllLines(@"2018/Input/Day1.txt");
            int answer = 0;

            foreach(var value in values)
            {
                answer += int.Parse(value);
            }

            Console.WriteLine($"Answer 1: {answer}");

            var frequencies = new HashSet<int>();
            answer = 0;
            var answerFound = false;

            do
            {
                foreach (var value in values)
                {
                    answer += int.Parse(value);

                    if (!frequencies.Add(answer))
                    {
                        Console.WriteLine($"Answer 2: {answer}");
                        answerFound = true;
                        return;
                    }
                }
            } while (!answerFound);

        }
    }
}
