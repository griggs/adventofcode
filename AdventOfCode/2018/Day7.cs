﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day7
    {
        public static void Run()
        {
            var input = File.ReadAllLines(@"../../../../Input/Day7.txt");
            var steps = new List<Step>();
            var steps2 = new List<Step>();
            foreach (var record in input)
            {
                var blocker = record.Substring(record.IndexOf("Step") + 5,1);
                var blocks = record.Substring(record.IndexOf("can") - 2,1);

                if (!steps.Any(x => x.Id == blocker))
                {
                    steps.Add(new Step() { Id = blocker });
                    steps2.Add(new Step() { Id = blocker });
                }

                if (!steps.Any(x => x.Id == blocks))
                {
                    steps.Add(new Step() { Id = blocks });
                    steps2.Add(new Step() { Id = blocks });
                }

                steps.Where(x => x.Id == blocks).First().Blockers.Add(blocker);
                steps2.Where(x => x.Id == blocks).First().Blockers.Add(blocker);
            }

            var answer1 = "";
            while(steps.Any(x => !x.Complete))
            {
                var stepToProcess = steps.Where(x => x.Blockers.Count == 0 && !x.Complete).OrderBy(x => x.Id).First();
                answer1 = $"{answer1}{stepToProcess.Id}";
                stepToProcess.Complete = true;
                steps.ForEach((x) => { x.Blockers.Remove(stepToProcess.Id); });
            }
            
            Console.WriteLine($"Answer 1: {answer1}");

            steps.ForEach(x => x.Complete = false);

            var answer2 = "";
            var time = 0;
            var workers = new List<Worker>();
            var workerCount = 5;

            while (steps2.Any(x => !x.Complete))
            {
                
                //check if done
                foreach (var worker in workers)
                {
                    worker.TimeToComplete--;

                    if (worker.TimeToComplete == 0)
                    {
                        steps2.Where(x => x.Id == worker.Id).First().Complete = true;
                        steps2.ForEach((x) => { x.Blockers.Remove(worker.Id); });
                        worker.Remove = true;
                        answer2 = $"{answer2}{worker.Id}";
                    }
                }

                workers.RemoveAll(x => x.Remove);
                
                var stepsToProcess = steps2.Where(x => x.Blockers.Count == 0 && !x.Complete).OrderBy(x => x.Id).ToList();

                foreach(var step in stepsToProcess.Where(x => !workers.Any(w => w.Id == x.Id)))
                {
                    if (workers.Count < workerCount)
                    {
                        workers.Add(new Worker() { Id = step.Id, TimeToComplete = 60 + (System.Text.Encoding.ASCII.GetBytes(step.Id)[0] - 64) });
                    }
                }

                time++;
            }

            Console.WriteLine($"Answer 2: {answer2}");
            Console.WriteLine($"Answer 2: {time - 1}");


        }

       
    }

    public class Worker
    {
        public string Id { get; set; }
        public int TimeToComplete { get; set; }
        public bool Remove { get; set; }
    }

    public class Step
    {
        public string Id { get; set; }
        public List<string> Blockers { get; set; } = new List<string>();    
        public bool Complete { get; set; }
        
    }


}
