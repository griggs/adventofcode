﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day4
    {
        public static void Run()
        {
            var input = File.ReadAllLines(@"../../../../Input/Day4.txt");
            var requests = input.Select(x => new GuardLog(x)).ToList();

            var guards = new List<Guard>();
            var currentGuard = new Guard();
            var startSleep = new DateTime();
            var endSleep = new DateTime();

            foreach (var log in requests.OrderBy(x => x.Date))
            {
                if(log.Log.Contains("#"))
                {
                    var currentID = int.Parse(log.Log.Substring(log.Log.IndexOf("#") + 1, log.Log.IndexOf("begins") - log.Log.IndexOf("#") - 1));

                    if (!guards.Any(x => x.ID == currentID))
                    {
                        currentGuard = new Guard() { ID = currentID };                        
                        guards.Add(currentGuard);
                    }
                    else
                    {
                        currentGuard = guards.Where(x => x.ID == currentID).SingleOrDefault();
                    }

                }
                else
                {
                    if (log.Log == "falls asleep") 
                    {
                        startSleep = log.Date;
                    }
                    else
                    {
                        endSleep = log.Date;

                        for(var start = startSleep; start < endSleep; start = start.AddMinutes(1))
                        {
                            currentGuard.Asleep.Add((start.Minute, log.Date));
                        }

                    }
                }
            }

            var totalCounts = new HashSet<(int guardId, int totalsleeps, int minute, int count)>();
            foreach (var guard in guards)
            {
                if(guard.Asleep.Count > 0)
                {
                    var counts = new HashSet<(int minute, int count)>();
                    guard.Asleep.GroupBy(x => x.Minute).ToList().ForEach(x => counts.Add((x.Key, x.Count())));
                    var bestMinute = counts.OrderByDescending(x => x.count).First();
                    totalCounts.Add((guard.ID, guard.Asleep.Count(), bestMinute.minute, bestMinute.count));
                }
                
            }

            var answer = totalCounts.OrderByDescending(x => x.totalsleeps).First();
            Console.WriteLine($"Answer 1: {answer.guardId * answer.minute}");

            answer = totalCounts.OrderByDescending(x => x.count).First();
            Console.WriteLine($"Answer 4: {answer.guardId * answer.minute}");

            Debugger.Break();
        }
               
    }

    public class Guard
    {
        public int ID { get; set; }
        public List<(int Minute, DateTime Date)> Asleep { get; set; } = new List<(int Minute, DateTime Date)>();
    }

    public class GuardLog
    {
        public string Input { get; set; }
        public DateTime Date { get; set; }
        public string Log { get; set; }   
        
        public GuardLog(string input)
        {
            Input = input;
            Date = DateTime.ParseExact(input.Substring(input.IndexOf("[") + 1, input.IndexOf("]") - 1), "yyyy-MM-dd HH:mm", null);
            Log = input.Substring(input.IndexOf("]") + 2, input.Length - input.IndexOf("]") - 2);
        }
    }
}
