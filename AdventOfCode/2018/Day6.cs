﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day6
    {
        public static void Run()
        {
            var input = File.ReadAllLines(@"../../../../Input/Day6.txt");
            var coords = input.Select(x => new Coordinate(x)).ToList();

            var minx = coords.OrderBy(x => x.Points.x).First().Points.x;
            var maxx = coords.OrderByDescending(x => x.Points.x).First().Points.x;
            var miny = coords.OrderBy(x => x.Points.y).First().Points.y;
            var maxy = coords.OrderByDescending(x => x.Points.y).First().Points.y;
            var counts = new List<(int x, int y)?>();

            //Set infinate
            foreach (var point in coords)
            {
                if(point.Points.x == minx || point.Points.x == maxx || point.Points.y == miny || point.Points.y == maxy)
                {
                    point.Infinate = true;
                }
            }

            for(var x = minx; x < maxx; x++)
            {
                for(var y = miny; y < maxy; y++)
                {
                    var point = GetClosestPoint((x, y), coords);
                    if(point != null)
                    {
                        counts.Add(point);
                    }
                    
                }
            }

            var grouped = counts.GroupBy(x => x);
            Console.WriteLine($"Answer 1 : {grouped.OrderByDescending(x => x.Count()).First().Count()}");

            var region = new List <(int x, int y)> ();
            for (var x = minx; x < maxx; x++)
            {
                for (var y = miny; y < maxy; y++)
                {
                    var totalDistance = CalculateTotalDistance((x, y), coords);

                    if(totalDistance < 10000)
                    {
                        region.Add((x, y));
                    }

                }
            }

            Console.WriteLine($"Answer 2: {region.Count()}");

            Debugger.Break();
            

        }

        private static int CalculateTotalDistance((int x, int y) p, List<Coordinate> coords)
        {
            var totalDistance = 0;
            foreach (var point in coords)
            {
                var distance = Math.Abs(p.x - point.Points.x) + Math.Abs(p.y - point.Points.y);
                totalDistance += distance;                
            }
            return totalDistance;
        }

        public static (int x, int y)? GetClosestPoint((int x, int y) p, List<Coordinate> coords)
        {
            //Short circuit if exact point
            var exact = coords.Where(x => x.Points.x == p.x && x.Points.y == p.y).FirstOrDefault();
            if (exact != null)
            {
                return (exact.Points.x, exact.Points.y);
            }

            Coordinate currentClosest = null;
            int distanceClosest = int.MaxValue;

            foreach(var point in coords)
            {
                var distance = Math.Abs(p.x - point.Points.x) + Math.Abs(p.y - point.Points.y);

                if(distance < distanceClosest)
                {
                    currentClosest = point;
                    distanceClosest = distance;
                }
                else if(distance == distanceClosest)
                {
                    currentClosest = null;
                }
            }

            if(!(currentClosest == null))
            {
                return (currentClosest.Points.x, currentClosest.Points.y);
            }

            return null;
        }
    }

    public class Coordinate
    {
        public (int x,int y) Points { get; set; }
        public bool Infinate { get; set; }
        public Coordinate(string x)
        {
            var a = x.Split(',');
            Points = (int.Parse(a[0]), int.Parse(a[1]));
        }
    }


}
