﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day12
    {
        public static void Run()
        {
            var input = File.ReadAllLines(@"../../../../Input/Day12.txt");
            //var input = File.ReadAllLines(@"../Input/Day12Test.txt");
            var firstLine = input.First();
            var stateInput = firstLine.Substring(firstLine.IndexOf("initial state: ") + 15);
            var rules = input.Where(x => x.Contains("=>")).Select(x => new Rule(x)).ToList();

            var pots = new List<Pot>();
            var lastPots = new List<Pot>();
            var lastValue = 0;
            var lastIncrease = 0;
            var sameAsLast = 0;
            for(var i=0; i<stateInput.Length;i++)
            {
                pots.Add(new Pot() { Index = i, Value = stateInput[i].ToString() });
            }

            Console.WriteLine($"{string.Concat(pots.Select(x => x.Value).ToArray())}");
            for (Int64 i=1;i<= 50000000000; i++)
            {

                if(i % 10000 == 0)
                {
                    Console.WriteLine($"Iteration: {i}");
                }
                var firstPlant = pots.First(x => x.Value == "#").Index;
                var lastPlant = pots.Last(x => x.Value == "#").Index;
                pots.RemoveAll(x => x.Index < firstPlant);
                pots.RemoveAll(x => x.Index > lastPlant);

                pots.Insert(0, new Pot() { Index = pots.First().Index - 1, Value = "." });
                pots.Insert(0, new Pot() { Index = pots.First().Index - 1, Value = "." });
                pots.Insert(0, new Pot() { Index = pots.First().Index - 1, Value = "." });
                pots.Insert(0, new Pot() { Index = pots.First().Index - 1, Value = "." });
                pots.Insert(0, new Pot() { Index = pots.First().Index - 1, Value = "." });
                pots.Add(new Pot() { Index = pots.Last().Index + 1, Value = "." });
                pots.Add(new Pot() { Index = pots.Last().Index + 1, Value = "." });
                pots.Add(new Pot() { Index = pots.Last().Index + 1, Value = "." });
                pots.Add(new Pot() { Index = pots.Last().Index + 1, Value = "." });
                pots.Add(new Pot() { Index = pots.Last().Index + 1, Value = "." });

                lastPots = pots.Select(x => new Pot(x)).ToList();
                pots.ForEach(x => x.Value = ".");

                for (var n=2;n< lastPots.Count-2;n++)
                {
                    //run rules
                    var rulesToRun = rules.Where(x => x.Current == lastPots[n].Value).ToList();
                    foreach(var rule in rulesToRun)
                    {
                        if(rule.One == lastPots[n-2].Value
                            && rule.Two == lastPots[n - 1].Value
                            && rule.Four == lastPots[n + 1].Value
                            && rule.Five == lastPots[n + 2].Value)
                        {
                            pots[n].Value = rule.Output;
                            break;
                        }
                    }                    
                }

                var currentValue = pots.Where(x => x.Value == "#").Sum(x => x.Index);
                var increase = currentValue - lastValue;

                if(increase == lastIncrease)
                {
                    sameAsLast++;
                }
                else
                {
                    sameAsLast = 0;
                }

                Console.WriteLine($"CurrentValue: {pots.Where(x => x.Value == "#").Sum(x => x.Index)}");
                Console.WriteLine($"LastValue: {lastValue}");
                Console.WriteLine($"Increase: {currentValue - lastValue}");

                if(sameAsLast == 5)
                {
                    Debugger.Break();
                }

                lastIncrease = increase;
                lastValue = pots.Where(x => x.Value == "#").Sum(x => x.Index);
                //Console.WriteLine($"{string.Concat(pots.Select(x => x.Value).ToArray())}");
            }

            Console.WriteLine($"Answer 1: {pots.Where(x => x.Value == "#").Sum(x => x.Index)}");

            //Debugger.Break();

        }
    }

    class Pot
    {
        public int Index { get; set; }
        public string Value { get; set; }

        public Pot()
        {

        }

        public Pot(Pot toCopy)
        {
            Index = toCopy.Index;
            Value = toCopy.Value;
        }
    }

    class Rule
    {
        public string Output { get; set; }
        public string Current { get; set; }
        public string One { get; set; }
        public string Two { get; set; }
        public string Four { get; set; }
        public string Five { get; set; }
        public List<(int postion, string value)> Ruleset { get; set; } = new List<(int postion, string value)>();

        public Rule(string line)
        {
            Output = line.Substring(line.IndexOf("=> ") + 3).Trim();
            Current = line.Substring(2, 1);
            One = line.Substring(0, 1);
            Two = line.Substring(1, 1);
            Four = line.Substring(3, 1);
            Five = line.Substring(4, 1);

            Ruleset.Add((-2, line.Substring(0,1)));
            Ruleset.Add((-1, line.Substring(1, 1)));
            Ruleset.Add((1, line.Substring(3, 1)));
            Ruleset.Add((2, line.Substring(4, 1)));
        }
    }
}
