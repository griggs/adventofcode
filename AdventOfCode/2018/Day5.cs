﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day5
    {
        public static void Run()
        {
            var input = File.ReadAllLines(@"../../../../Input/Day5.txt");
            var line = input.First();
            var newLine = Condense(line);
            Console.WriteLine($"Answer 1 : {newLine.Length}");


            var alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            var counts = new List<(char Letter, int Count)>();
            foreach(var a in alpha)
            {
                var condensed = Condense(line.Replace(a.ToString(), "", StringComparison.OrdinalIgnoreCase));
                counts.Add((a, condensed.Length));
            }

            Console.WriteLine($"Answer 2 : {counts.OrderBy(x => x.Count).First().Count}");
                       

        }

        static string Condense(string line)
        {
            for (var i = 0; i < line.Length - 1; i++)
            {

                if (line[i] != line[i + 1] && char.ToUpper(line[i]) == char.ToUpper(line[i + 1]))
                {
                    line = line.Remove(i, 2);
                    i = i - 2;
                    if (i <= 0) i = -1;
                }

            }

            return line;
        }
               
    }


}
