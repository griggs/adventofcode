﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
    public static class Day8
    {
        static int position;
        static List<Node> nodes = new List<Node>();

        static string[] items;
        public static void Run()
        {
            //var input = File.ReadAllLines(@"../../../../Input/Day8Test.txt");
            var input = File.ReadAllLines(@"../Input/Day8.txt");
            var line = input.First();
            items = line.Split(" ");

            var rootNode = Calculate();

            Console.WriteLine($"Answer 1:{rootNode.Sum()}");
            Console.WriteLine($"Answer 2:{rootNode.Value()}");


        }

        static Node Calculate()
        {
            var node = new Node();
            node.ChildrenCount = int.Parse(items[position++]);
            node.MetaDataCount = int.Parse(items[position++]);

            for (var i = 0; i < node.ChildrenCount; i++)
            {

                node.Children.Add(Calculate());
            }

            for (var i = 0; i < node.MetaDataCount; i++)
            {
                node.MetaData.Add(int.Parse(items[position++]));
            }

            return node;

        }


    }

    class Node
    {
        public int ChildrenCount { get; set; }
        public int MetaDataCount { get; set; }
        public List<int> MetaData { get; set; } = new List<int>();
        public List<Node> Children { get; set; } = new List<Node>();

        public int Sum() => MetaData.Sum() + Children.Sum(x => x.Sum());
        public int Value()
        {
            if (!Children.Any())
            {
                return MetaData.Sum();
            }

            var value = 0;
            foreach (var m in MetaData)
            {
                if (m <= Children.Count)
                {
                    value += Children[m - 1].Value();
                }
            }

            return value;
        }
    }




}
